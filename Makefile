memmanage: memmanage.o
	g++ -Wall -pedantic -ansi -g -O2 -o memmanage memmanage.o
memmanage.o: memmanage.cpp
	g++ -Wall -pedantic -ansi -g -O2 -c memmanage.cpp

.PHONY: clean
clean:
	rm -f memmanage
